/*
 * This is a custom implementation of the Collector interface.
 * Statistics are objects gather max,min,sum,average statistics.
 */


import java.util.Comparator;
import java.util.EnumSet;
import java.util.Set;
import java.util.function.BiConsumer;
import java.util.function.BinaryOperator;
import java.util.function.Function;
import java.util.function.Supplier;
import java.util.stream.Collector;

import static java.lang.Double.parseDouble;

public class Statistics implements Collector<String, Statistics, Statistics> {

/**
 * This implementation does not need to be thread safe because
 * the parallel implementation of
 * {@link java.util.stream.Stream#collect Stream.collect()}
 * provides the necessary partitioning and isolation for safe parallel
 * execution.
 */
    private String maxRecord;
    private String minRecord;

    private double sum;
    private int lineCount;
    private final BinaryOperator<String> maxOperator;
    private final BinaryOperator<String> minOperator;
    private final int column;

    public Statistics(int column) {
        this.column = column;
        Comparator<String> cmp = Comparator.comparing(
                (String str) -> parseDouble(getCell(str, column)));
        maxOperator = BinaryOperator.maxBy(cmp);
        minOperator = BinaryOperator.minBy(cmp);
    }

    /*
     * Process line.
     */
    public Statistics accept(String line) {
        maxRecord = maxRecord == null
                ? line : maxOperator.apply(maxRecord, line);
        minRecord = minRecord == null
                ? line : minOperator.apply(minRecord, line);

        sum += parseDouble(getCell(line, column));
        lineCount++;
        return this;
    }


    /*
     * Merge two Statistics.
     */
    public Statistics combine(Statistics stat) {
        maxRecord = maxOperator.apply(maxRecord, stat.getMaxRecord());
        minRecord = minOperator.apply(minRecord, stat.getMinRecord());
        sum += stat.getSum();
        lineCount += stat.getLineCount();
        return this;
    }

    private static String getCell(String record, int column) {
        return record.split(",")[column].trim();
    }

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append("#------Statistics------#\n");
        sb.append("Max: ").append(getMaxRecord()).append("\n");
        sb.append("Min: ").append(getMinRecord()).append("\n");
        sb.append("Sum = ").append(getSum()).append("\n");
        sb.append("Average = ").append(average()).append("\n");
        sb.append("#------Statistics------#\n");
        return sb.toString();
    }

    @Override
    public Supplier<Statistics> supplier() {
        return () -> new Statistics(column);
    }

    @Override
    public BiConsumer<Statistics, String> accumulator() {
        return Statistics::accept;
    }

    @Override
    public BinaryOperator<Statistics> combiner() {
        return Statistics::combine;

    }

    @Override
    public Function<Statistics, Statistics> finisher() {
        return stat -> stat;
    }

    @Override
    public Set<Characteristics> characteristics() {
        return EnumSet.of(Characteristics.IDENTITY_FINISH);
    }

    private String getMaxRecord() {
        return maxRecord;
    }

    private String getMinRecord() {
        return minRecord;
    }

    private double getSum() {
        return sum;
    }

    private double average() {
        return sum / lineCount;
    }

    private int getLineCount() {
        return lineCount;
    }

}

