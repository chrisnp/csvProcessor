import java.io.BufferedReader;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.*;
import java.util.function.Predicate;
import java.util.regex.Pattern;
import java.util.stream.Collectors;

import static java.lang.Double.parseDouble;
import static java.util.stream.Collectors.*;

/**
 * CSVProcessor is a tool for processing CSV files. There are several
 * command-line options. Consult the {@link #printUsageAndExit} method for
 * instructions and command line parameters. This sample shows examples of the
 * following features:
 * <ul>
 * <li>Lambda and bulk operations. Working with streams: map(...), filter(...),
 * sorted(...) methods. The collect(...) method with different collectors:
 * Collectors.maxBy(...), Collectors.minBy(...), Collectors.toList(),
 * Collectors.toCollection(...), Collectors.groupingBy(...),
 * Collectors.toDoubleSummaryStatistics(...), and a custom Collector.</li>
 * <li>Static method reference for printing values.</li>
 * <li>Try-with-resources feature for closing files.</li>
 * <li>Switch by String feature.</li>
 * <li>Other new APIs: Pattern.asPredicate(), BinaryOperator
 * BufferedReader.lines(), Collection.forEach(...), Comparator.comparing(...),
 * Comparator.reversed(), Arrays.stream(...).</li>
 * </ul>
 *
 */

public class CSVProcessor {

    //Number of characters that may be read
    private static final int READ_AHEAD_LIMIT = 100_000_000;

    private static void statInSeveralPasses(BufferedReader br, int column)
            throws IOException {
        System.out.println("#-----Statistics in several passes-------#");
        //Create a comparator to compare records by the column.
        Comparator<String> comparator
                = Comparator.comparing(
                (String str) -> parseDouble(getCell(str, column)));
        //Find max record by using Collectors.maxBy(...)
        System.out.println(
                "Max: " + br.lines().collect(maxBy(comparator)).get());
        br.reset();
        //Find min record by using Collectors.minBy(...)
        System.out.println(
                "Min: " + br.lines().collect(minBy(comparator)).get());
        br.reset();
        //Compute the average value and sum with
        //Collectors.toDoubleSummaryStatistics(...)
        DoubleSummaryStatistics doubleSummaryStatistics
                = br.lines().collect(summarizingDouble(
                str -> parseDouble(getCell(str, column))));
        System.out.println("Average: " + doubleSummaryStatistics.getAverage());
        System.out.println("Sum: " + doubleSummaryStatistics.getSum());
    }

    private static void verifyArgumentNumber(String[] args, int n) {
        if (args.length != n) {
            printUsageAndExit("Expected " + n + " arguments but was "
                    + args.length);
        }
    }

    private static int getColumnNumber(List<String> header, String name) {
        int column = header.indexOf(name);
        if (column == -1) {
            printUsageAndExit("There is no column with name " + name);
        }
        return column;
    }

    private static String getCell(String record, int column) {
        return record.split(",")[column].trim();
    }

    private static void printUsageAndExit(String... str) {
        System.out.println("Usages:");

        System.out.println("CSVProcessor sort COLUMN_NAME ASC|DESC FILE");
        System.out.println("Sort lines by column COLUMN_NAME in CSV FILE\n");

        System.out.println("CSVProcessor search COLUMN_NAME REGEX FILE");
        System.out.println("Search for REGEX in column COLUMN_NAME in CSV FILE\n");

        System.out.println("CSVProcessor groupby COLUMN_NAME FILE");
        System.out.println("Split lines into different groups according to column "
                + "COLUMN_NAME value\n");

        System.out.println("CSVProcessor stat COLUMN_NAME FILE");
        System.out.println("Compute max/min/average/sum  statistics by column "
                + "COLUMN_NAME\n");

        Arrays.asList(str).forEach(System.err::println);
        System.exit(1);
    }


    /**
     * The main method for the CSVProcessor program. Run the program with an
     * empty argument list to see possible arguments.
     *
     * @param args the argument list for CSVProcessor.
     */
    public static void main(String[] args) {
        if (args.length < 2) {
            printUsageAndExit();
        }
        try (BufferedReader br = new BufferedReader(
                Files.newBufferedReader(Paths.get(args[args.length - 1])))) {
            //Assume that the first line contains column names.
            List<String> header = Arrays.stream(br.readLine().split(","))
                    .map(String::trim).collect(toList());
            //Calculate an index of the column in question.
            int column = getColumnNumber(header, args[1]);
            switch (args[0]) {
                case "sort":
                    verifyArgumentNumber(args, 4);
                    //Define the sort order.
                    boolean isAsc;
                    switch (args[2].toUpperCase()) {
                        case "ASC":
                            isAsc = true;
                            break;
                        case "DESC":
                            isAsc = false;
                            break;
                        default:
                            printUsageAndExit("Illegal argument" + args[2]);
                            return;//Should not be reached.
                    }
                    /*
                     * Create a comparator that compares lines by comparing
                     * values in the specified column.
                     */
                    Comparator<String> cmp
                            = Comparator.comparing(str -> getCell(str, column),
                            String.CASE_INSENSITIVE_ORDER);
                    /*
                     * sorted(...) is used to sort records.
                     * forEach(...) is used to output sorted records.
                     */
                    br.lines().sorted(isAsc ? cmp : cmp.reversed())
                            .forEach(System.out::println);
                    break;
                case "search":
                    verifyArgumentNumber(args, 4);
                    /*
                     * Records are filtered by a regex.
                     * forEach(...) is used to output filtered records.
                     */
                    Predicate<String> pattern
                            = Pattern.compile(args[2]).asPredicate();
                    br.lines().filter(str -> pattern.test(getCell(str, column)))
                            .forEach(System.out::println);
                    break;
                case "groupby":
                    verifyArgumentNumber(args, 3);
                    /*
                     * Group lines by values in the column with collect(...), and
                     * print with forEach(...) for every distinct value within
                     * the column.
                     */
                    br.lines().collect(
                            Collectors.groupingBy(str -> getCell(str, column),
                                    toCollection(TreeSet::new)))
                            .forEach((str, set) -> {
                                System.out.println(str + ":");
                                set.forEach(System.out::println);
                            });
                    break;
                case "stat":
                    verifyArgumentNumber(args, 3);

                    /*
                     * BufferedReader will be read several times.
                     * Mark this point to return here after each pass.
                     * BufferedReader will be read right after the headers line
                     * because it is already read.
                     */
                    br.mark(READ_AHEAD_LIMIT);

                    /*
                     * Statistics can be collected by a custom collector in one
                     * pass. One pass is preferable.
                     */
                    System.out.println(
                            br.lines().collect(new Statistics(column)));

                    /*
                     * Alternatively, statistics can be collected
                     * by a built-in API in several passes.
                     * This method demonstrates how separate operations can be
                     * implemented using a built-in API.
                     */
                    br.reset();
                    statInSeveralPasses(br, column);
                    break;
                default:
                    printUsageAndExit("Illegal argument" + args[0]);
            }
        } catch (IOException e) {
            printUsageAndExit(e.toString());
        }
    }

}
